package com.jl.shudu.activity

import android.text.TextUtils
import cn.bmob.v3.BmobUser
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.SaveListener
import com.admin.inspecting.until.SharedPreferencesUntil
import com.jl.shudu.R
import com.jl.shudu.base.BaseActivity
import com.jl.shudu.utils.Constant
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

/**
 * 作者： 吴昶 .
 * 时间: 2018/11/6 18:12
 * 功能简介：登录页面
 */
class LoginActivity: BaseActivity(){

    override fun bindLayout(): Int {
        return R.layout.activity_login
    }

    override fun initData() {
        setStatusBarFullScreen()
    }

    override fun initEvent() {
        btn_login.setOnClickListener {
            val user=edt_user_name.text.toString()
            if(TextUtils.isEmpty(user)){
                toast("请输入用户名!")
                return@setOnClickListener
            }
            val pass=edt_pass.text.toString()
            if(TextUtils.isEmpty(pass)){
                toast("请输入密码!")
                return@setOnClickListener
            }
            val usert=BmobUser()
            usert.username=user
            usert.setPassword(pass)
            usert.login(object:SaveListener<BmobUser>(){
                override fun done(p0: BmobUser?, p1: BmobException?) {
                    if(p1==null){
                        SharedPreferencesUntil.saveValue(Constant.USER_NAME,user)
                        SharedPreferencesUntil.saveValue(Constant.USER_ID,p0!!.objectId)
                        startActivity<MainActivity>()
                        finish()
                    }else{
                        toast("${p1.message}")
                    }
                }
            })

        }

        iv_clear_user_name.setOnClickListener {
            edt_user_name.setText("")
        }

        tv_register.setOnClickListener {
            RegisterActivity.startRegisterActivity(this)
        }

    }

}