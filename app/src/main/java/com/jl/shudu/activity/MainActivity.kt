package com.jl.shudu.activity

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import android.view.WindowManager
import cn.bmob.v3.BmobQuery
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.FindListener
import cn.bmob.v3.listener.SaveListener
import com.admin.inspecting.until.SharedPreferencesUntil
import com.jl.shudu.R
import com.jl.shudu.base.BaseActivity
import com.jl.shudu.table.CustomPass
import com.jl.shudu.utils.Constant
import com.jl.shudu.utils.ShareUtil
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity

class MainActivity : BaseActivity(){

    private var pass=0
    private var objectId=""

    override fun bindLayout(): Int {
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        return R.layout.activity_main
    }

    override fun initData() {

    }

    override fun initEvent() {
        btn_simple.setOnClickListener {
            startGame(6)
        }
        btn_midd.setOnClickListener {
            startGame(5)
        }
        btn_d.setOnClickListener {
            startGame(4)
        }
        tv_share.setOnClickListener {
            showShare()
        }
        tv_setting.setOnClickListener {
            startActivity<SettingActivity>()
        }
    }

    private fun startGame(smd:Int){
        startActivity<GameActivity>("smd" to smd,"pass" to pass,"objectId" to objectId)
    }

    private fun showShare() {
        ShareUtil.showMsg(this,
            Constant.SHARE_TITLE,
            Constant.SHARE_TITLE_URL,
            Constant.SHARE_TEXT,
            Constant.SHARE_URL,
            Constant.SHARE_IMAGE_URL)
    }

    @SuppressLint("SetTextI18n")
    override fun onResume() {
        super.onResume()
        getHasPass()
    }

    private fun getHasPass(){
        val id=SharedPreferencesUntil.getValue(Constant.USER_ID,"").toString()
        val query=BmobQuery<CustomPass>()
        query.addWhereEqualTo("userId",id)
        query.findObjects(object :FindListener<CustomPass>(){
            override fun done(p0: MutableList<CustomPass>?, p1: BmobException?) {
                linear_game.visibility= View.VISIBLE
                if(p1==null){
                    if(p0!!.size==0){
                        tv_break.text="您还未开始游戏"
                        Log.d("********","no one")
                        val customPass=CustomPass()
                        customPass.userId=id
                        customPass.passNumber=0
                        customPass.save(object :SaveListener<String>(){
                            override fun done(p0: String?, p1: BmobException?) {
                                if(p1==null){
                                    objectId=p0!!
                                    Log.d("********",p0)
                                }else{
                                    Log.d("********",p1.message)
                                }
                            }
                        })
                    }else{
                        Log.d("********","has pass")
                        objectId=p0[0].objectId
                        pass=p0[0].passNumber
                        tv_break.text=if(pass==0){ "您还未开始游戏" }else{ "您已完成 $pass 局游戏" }
                    }
                }else{
                    Log.d("********","no pass")
                    tv_break.text="您还未开始游戏"
                }
            }
        })
    }

}
