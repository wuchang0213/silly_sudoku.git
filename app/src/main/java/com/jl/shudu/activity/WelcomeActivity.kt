package com.jl.shudu.activity

import android.os.Handler
import android.text.TextUtils
import android.util.Log
import com.admin.inspecting.until.SharedPreferencesUntil
import com.jl.shudu.R
import com.jl.shudu.base.BaseActivity
import com.jl.shudu.utils.Constant
import org.jetbrains.anko.startActivity

/**
 * 作者： 吴昶 .
 * 时间: 2018/12/3 15:05
 * 功能简介：
 */
class WelcomeActivity:BaseActivity(){

    val handler=Handler()

    override fun bindLayout(): Int {
        return R.layout.activity_welcome
    }

    override fun initData() {

    }

    override fun initEvent() {

    }

    override fun onResume() {
        super.onResume()
        handler.postDelayed({
            val name=SharedPreferencesUntil.getValue(Constant.USER_NAME,"").toString()
            Log.d("********",name)
            if(TextUtils.isEmpty(name)){
                startActivity<LoginActivity>()
            }else{
                startActivity<MainActivity>()
            }
            finish()
        },1000)
    }

}