package com.jl.shudu.activity

import android.graphics.Bitmap
import android.graphics.Canvas
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.WindowManager
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.UpdateListener
import com.admin.inspecting.until.SharedPreferencesUntil
import com.jl.shudu.R
import com.jl.shudu.base.BaseActivity
import com.jl.shudu.dialog.FinishDialog
import com.jl.shudu.interfaces.NumberClickListener
import com.jl.shudu.interfaces.OnFinishClickListener
import com.jl.shudu.table.CustomPass
import com.jl.shudu.utils.Constant
import com.jl.shudu.utils.ShuduData
import kotlinx.android.synthetic.main.activity_game.*
import org.jetbrains.anko.toast
import java.util.*

/**
 * 作者： 吴昶 .
 * 时间: 2018/11/27 10:25
 * 功能简介：
 */
class GameActivity : BaseActivity() , NumberClickListener,
    OnFinishClickListener {

    private var data=Array(9) { IntArray(9) }
    private val random = Random()
    private var isFist=true
    private var smd=4
    private var finishDialog: FinishDialog?=null
    private var pass=0
    private var objectId=""

    private var hasFinish=false

    override fun bindLayout(): Int {
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        return R.layout.activity_game
    }

    override fun initData() {
        tv_shudu_title.setCallBackListener { finish() }
        smd=intent.getIntExtra("smd",4)
        pass=intent.getIntExtra("pass",0)
        objectId=intent.getStringExtra("objectId")
        finishDialog= FinishDialog(this, this)
    }

    override fun initEvent() {
        nv_01.setOnClickListener {
            nv_01.invalidate()
        }

        numberClick(btn_1,1)
        numberClick(btn_2,2)
        numberClick(btn_3,3)
        numberClick(btn_4,4)
        numberClick(btn_5,5)
        numberClick(btn_6,6)
        numberClick(btn_7,7)
        numberClick(btn_8,8)
        numberClick(btn_9,9)
        numberClick(btn_0,0)
        nv_01.setListener(this)

        btn_finish.setOnClickListener {
            nv_01.checkResult()
        }
        btn_next.setOnClickListener {
            if(!hasFinish){
                toast("请先完成本局")
            }else{
                goNextBreak()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (isFist){
            isFist=false
            initSize()
        }
    }

    override fun onClickIndex(x: Int, y: Int) {

    }
    override fun onOrigialCanNotChange() {

    }

    override fun onResultIsTrue(isTrue: Boolean,errorcode:Int) {
        if(errorcode==-1){
            toast("您还未完成")
            return
        }
        hasFinish=isTrue
        nv_01.invalidate()
        if(isTrue){
            if(!TextUtils.isEmpty(objectId)){
                pass++
                val customPass= CustomPass()
                customPass.userId= SharedPreferencesUntil.getValue(Constant.USER_ID,"").toString()
                customPass.objectId=objectId
                customPass.passNumber=pass
                customPass.update(object: UpdateListener(){
                    override fun done(p0: BmobException?) {
                        if(p0!=null){
                            Log.d("****",p0.message)
                        }
                    }
                })
            }
            finishDialog!!.setResultBitmap(getGameResult())
            finishDialog!!.show()
        }else{

        }
    }

    private fun numberClick(view: View, number:Int){
        view.setOnClickListener {
            nv_01.setNewNumber(number)
        }
    }

    /**
     * 初始化数据
     */
    private fun initSize(){
        data= emptyArray()
        data= ShuduData().generateShuDu()
        val initData=Array(9) { IntArray(9) }
        for(i in 0 until 9){
            for(j in 0 until smd){
                val ranNum = random.nextInt(9)
                initData[i][ranNum]=data[i][ranNum]
            }
        }
        nv_01.number=initData
    }

    override fun onFinishClick() {
        finish()
    }

    override fun onNextClick() {
        goNextBreak()
    }

    override fun onCancel() {

    }

    private fun goNextBreak(){
        hasFinish=false
        data= emptyArray()
        nv_01.clear()
        initSize()
    }

    private fun getGameResult():Bitmap{
        val bitmap=Bitmap.createBitmap(nv_01.width,nv_01.height,Bitmap.Config.ARGB_8888)
        val canvas=Canvas(bitmap)
        nv_01.draw(canvas)
        return bitmap
    }

}
