package com.jl.shudu.activity

import cn.bmob.v3.BmobUser
import com.admin.inspecting.until.SharedPreferencesUntil
import com.jl.shudu.MyApplication
import com.jl.shudu.R
import com.jl.shudu.base.BaseActivity
import com.jl.shudu.utils.Constant
import kotlinx.android.synthetic.main.activity_setting.*
import org.jetbrains.anko.startActivity

/**
 * 作者： 吴昶 .
 * 时间: 2018/12/4 13:15
 * 功能简介：
 */
class SettingActivity:BaseActivity(){
    override fun bindLayout(): Int {
        return R.layout.activity_setting
    }

    override fun initData() {

    }

    override fun initEvent() {
        tv_setting_title.setCallBackListener { finish() }

        btn_login_out.setOnClickListener {
            BmobUser.logOut()
            MyApplication.cancelActivity()
            SharedPreferencesUntil.saveValue(Constant.USER_NAME,"")
            SharedPreferencesUntil.saveValue(Constant.USER_ID,"")
            startActivity<LoginActivity>()
            finish()
        }
    }

}