package com.admin.inspecting.until

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences

/**
 * Created by 吴昶 on 2018/8/22.
 */
class SharedPreferencesUntil{
    companion object {
        private var share: SharedPreferences?=null

        fun initShare(context:Context){
            share=context.getSharedPreferences("Inspecting", Activity.MODE_PRIVATE)
        }

        fun saveValue(key:String, value:Any){
            when (value) {
                is Int -> share!!.edit().putInt(key,value).apply()
                is String -> share!!.edit().putString(key,value).apply()
                is Boolean -> share!!.edit().putBoolean(key,value).apply()
                is Float -> share!!.edit().putFloat(key,value).apply()
                is Long->share!!.edit().putLong(key,value).apply()
            }
        }

        @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
        fun getValue(key: String, value: Any):Any{
            when(value){
                is Int ->{ return share!!.getInt(key,value) }
                is String ->{ return share!!.getString(key,value) }
                is Boolean ->{ return share!!.getBoolean(key,value) }
                is Float ->{ return share!!.getFloat(key,value) }
                is Long->{ return share!!.getLong(key,value) }
            }
            return value
        }


    }
}