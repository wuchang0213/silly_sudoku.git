package com.jl.shudu.utils

import android.content.Context
import android.text.TextUtils
import cn.sharesdk.onekeyshare.OnekeyShare

/**
 * 作者： 吴昶 .
 * 时间: 2018/12/3 15:24
 * 功能简介：
 */
object ShareUtil{

    fun showMsg(context: Context,title:String,titleUrl: String,text:String,url:String,imageUrl:String){
        val oks = OnekeyShare()
        oks.disableSSOWhenAuthorize()
        if(!TextUtils.isEmpty(title)){ oks.setTitle(title) }
        if(!TextUtils.isEmpty(titleUrl)){ oks.setTitleUrl(titleUrl) }
        if(!TextUtils.isEmpty(text)){ oks.text = text }
        if(!TextUtils.isEmpty(url)){ oks.setUrl(url) }
        if(!TextUtils.isEmpty(imageUrl)){oks.setImageUrl(imageUrl)}
        oks.show(context)
    }

}