package com.jl.shudu.interfaces

/**
 * 作者： 吴昶 .
 * 时间: 2018/11/27 15:02
 * 功能简介：
 */
interface OnFinishClickListener{
    fun onFinishClick()
    fun onNextClick()
    fun onCancel()
}