package com.jl.shudu.interfaces;

/**
 * 作者： 吴昶 .
 * 时间: 2018/11/26 17:01
 * 功能简介：
 */
public interface NumberClickListener {
    //x,y为数组的下标
    void onClickIndex(int x,int y);

    void onOrigialCanNotChange();

    void onResultIsTrue(boolean isTrue,int errorcode);
}
