package com.jl.shudu.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import com.jl.shudu.R
import com.jl.shudu.interfaces.OnFinishClickListener
import kotlinx.android.synthetic.main.dialog_finish.view.*

/**
 * 作者： 吴昶 .
 * 时间: 2018/11/27 14:59
 * 功能简介：
 */
class FinishDialog(context: Context,private var listener: OnFinishClickListener):Dialog(context,
    R.style.dialog_FullScreen){
    private var fview:View?=null

    init {
        fview=LayoutInflater.from(context).inflate(R.layout.dialog_finish,null)
        fview!!.btn_dialog_finish.setOnClickListener {
            cancel()
            listener.onFinishClick() }
        fview!!.btn_dialog_next.setOnClickListener {
            cancel()
            listener.onNextClick() }
        fview!!.btn_dialog_cancel.setOnClickListener {
            cancel()
            listener.onCancel()
        }
        fview!!.tv_prompt.text="恭喜完成一局"
        setCanceledOnTouchOutside(false)
        setContentView(fview!!)
    }

    fun setResultBitmap(bitmap: Bitmap){
        fview!!.iv_game_result.setImageBitmap(bitmap)
    }
}