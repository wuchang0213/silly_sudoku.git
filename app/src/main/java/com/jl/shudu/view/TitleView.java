package com.jl.shudu.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.jl.shudu.R;

/**
 * Created by 吴昶 on 2018/9/18.
 */
public class TitleView extends LinearLayout {

    private TextView title;
    private ImageView callBack;
    private TextView option;
    private ImageView iv_option;
    private LinearLayout back;
    private String titleMsg;
    private int color;
    private int titleColor;

    public TitleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.layout_title_can_back,this,true);
        TypedArray typedArray=context.obtainStyledAttributes(attrs, R.styleable.TitleView);
        titleMsg=typedArray.getString(R.styleable.TitleView_title);
        color=typedArray.getColor(R.styleable.TitleView_backgroundColor,getResources().getColor(R.color.transparent));
        titleColor=typedArray.getColor(R.styleable.TitleView_title_text_color,getResources().getColor(R.color.main_title));
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        title=(TextView)findViewById(R.id.tv_back_title);
        callBack=(ImageView)findViewById(R.id.iv_back);
        option=(TextView)findViewById(R.id.tv_back_option);
        iv_option=(ImageView)findViewById(R.id.iv_option);
        back=(LinearLayout)findViewById(R.id.linear_iv_back);
        if(!TextUtils.isEmpty(titleMsg)){
            title.setText(titleMsg);
        }
        setBackgroundColor(color);
        title.setTextColor(titleColor);
    }

    public TitleView setTitle(String messg){
        title.setText(messg);
        return this;
    }

    public TitleView hiddenBack(){
        callBack.setVisibility(GONE);
        return this;
    }

    public TitleView setBackground(int color){
        setBackgroundColor(getResources().getColor(color));
        return this;
    }

    public TitleView setCallBackListener(OnClickListener listener){
        callBack.setOnClickListener(listener);
        back.setOnClickListener(listener);
        return this;
    }

    public TitleView setOpTionBackground(int res){
        iv_option.setImageResource(res);
        return this;
    }

    public TitleView setOpTionClickListener(OnClickListener listener){
        iv_option.setOnClickListener(listener);
        option.setOnClickListener(listener);
        return this;
    }

    public TitleView setOptionText(String options){
        option.setText(options);
        return this;
    }
}
