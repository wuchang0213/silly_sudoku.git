package com.jl.shudu.table;

import cn.bmob.v3.BmobUser;

/**
 * 作者： 吴昶 .
 * 时间: 2018/12/3 15:17
 * 功能简介：
 */
public class User extends BmobUser {

    String phone;
    String secret;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
