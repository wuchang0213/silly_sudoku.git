package com.jl.shudu.table;

import cn.bmob.v3.BmobObject;

/**
 * 作者： 吴昶 .
 * 时间: 2018/12/3 15:50
 * 功能简介：
 */
public class CustomPass extends BmobObject {

    String userId;
    int passNumber;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getPassNumber() {
        return passNumber;
    }

    public void setPassNumber(int passNumber) {
        this.passNumber = passNumber;
    }
}
