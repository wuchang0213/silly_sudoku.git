package com.jl.shudu

import android.app.Activity
import android.app.Application
import cn.bmob.v3.Bmob
import com.admin.inspecting.until.SharedPreferencesUntil
import com.mob.MobSDK

/**
 * 作者： 吴昶 .
 * 时间: 2018/11/28 13:14
 * 功能简介：
 */
class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        MobSDK.init(this)
        SharedPreferencesUntil.initShare(this)
        Bmob.initialize(this,"da6abcd1d900474101ac95b00f7244aa")
    }

    companion object {
        val activitys=ArrayList<Activity>()

        fun addActivity(activity: Activity){
            activitys.add(activity)
        }

        fun cancelActivity(){
            for(activity in activitys){
                activity.finish()
            }
        }
    }



}
