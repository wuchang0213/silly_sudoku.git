package com.jl.shudu.base

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Window
import android.view.WindowManager
import com.jl.shudu.MyApplication

/**
 * 作者： 吴昶 .
 * 时间: 2018/11/6 15:56
 * 功能简介：所有的 Activity 的基类
 */
abstract class BaseActivity:AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(Window.FEATURE_NO_TITLE)
//        window.setFlags(WindowManager.LayoutParams.FLAG_SECURE,WindowManager.LayoutParams.FLAG_SECURE)
        MyApplication.addActivity(this)
        setContentView(bindLayout())
        initData()
        initEvent()
    }

    /**
     * 绑定 资源布局文件
     */
    abstract fun bindLayout():Int

    /**
     * 初始化 数据
     */
    abstract fun initData()

    /**
     * 初始化 响应事件
     */
    abstract fun initEvent()

    /**
     * 设置状态了颜色
     */
    open fun setStatusBarColor(color:Int){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
                && Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            window.statusBarColor = resources.getColor(color)
        }else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            window.statusBarColor = resources.getColor(color,null)
        }
    }

    /**
     * 设置全屏
     */
    open fun setStatusBarFullScreen(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //需要设置这个 flag 才能调用 setStatusBarColor 来设置状态栏颜色
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            //状态栏透明颜色
            window.statusBarColor = Color.TRANSPARENT
        }
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

    fun checkNull(message:String?):String{
        return message?:""
    }

}